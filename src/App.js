import React from 'react';
import './App.css';

import List from './components/List';

function App() {
    const [listsQuantity, setListsQuantity] = React.useState([1]);

    const listDelete = (index) => {
        setListsQuantity(listsQuantity.filter((list) => list !== index));
    };

    return (
        <div>
            {listsQuantity.map((list, index) => {
                return (
                    <div key={list + index}>
                        <List index={list} listDelete={() => listDelete(list)} />
                    </div>
                );
            })}
            <button
                onClick={() => setListsQuantity((oldArray) => [...oldArray, Math.random() * 1])}>
                Add
            </button>
        </div>
    );
}

export default App;
