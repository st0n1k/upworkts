import React from 'react';

const labels = [
    { pk: 2, label: 'Delivery', unit: null, currency: { pk: 1, name: 'Dollar', code: 'USD' } },
    { pk: 11, label: 'Hours', unit: { pk: 5, name: 'Hour', description: '' }, currency: null },
    { pk: 5, label: 'Margin', unit: { pk: 2, name: '%', description: '' }, currency: null },
];

const List = ({ index, listDelete }) => {
    const selectedEl = React.useRef(null);
    const [selectedValue, setSelectedValue] = React.useState('USD');

    const onChangeIt = React.useCallback(() => {
        setSelectedValue(selectedEl.current.value);
    }, []);

    return (
        <div>
            <select ref={selectedEl} name="" id="" onChange={onChangeIt}>
                {labels.map((label) => {
                    return (
                        <option
                            key={label.pk}
                            value={label.unit ? label.unit.name : label.currency.code}>
                            {label.label}
                        </option>
                    );
                })}
            </select>
            <input type="text" /> {selectedValue}
            <button
                onClick={(e) => {
                    e.preventDefault();
                    listDelete(index);
                }}>
                X
            </button>
        </div>
    );
};

export default List;
